## Test environments

* ubuntu 18.04 (on gitlab-ci), R 4.0.0
* win-builder (devel and release)

## R CMD check results

0 errors | 0 warnings | 0 note


---

This is the first release of racled.

Thanks!
Ahmadou Dicko
