
<!-- README.md is generated from README.Rmd. Please edit that file -->

# racled

[![GitLab CI Build
Status](https://gitlab.com/dickoa/racled/badges/master/pipeline.svg)](https://gitlab.com/dickoa/racled/pipelines)
[![AppVeyor build
status](https://ci.appveyor.com/api/projects/status/gitlab/dickoa/racled?branch=master&svg=true)](https://ci.appveyor.com/project/dickoa/racled)
<!-- [![Codecov Code Coverage](https://codecov.io/gl/dickoa/racled/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/racled) -->
[![CRAN
status](https://www.r-pkg.org/badges/version/racled)](https://cran.r-project.org/package=racled)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

`racled` is an R package to access from the [Armed Conflict Location and
Event Data Project](https://www.acleddata.com/) (ACLED) data.

## Install racled

You can install the development version from GitLab using the `remotes`
R package:

``` r
remotes::install_gitlab("dickoa/racled")
```

## racled: A quick tutorial

The `racled` package requires you to set your API access key and specify
asssociated email. In order to get your access key, you’ll need to
register by visiting:

<https://developer.acleddata.com/>

With your credential, the easiest way to setup `racled` is to to store
them in your `.Renviron` file which is automatically read by `R` on
startup.

You can either edit directly the `.Renviron` file or access it by
calling `usethis::edit_r_environ()` (assuming you have the `usethis`
package installed) and entering:

``` bash
ACLED_ACCESS_KEY="****************1234"
ACLED_EMAIL="you@youremail.com"
```

Or use directly the `racled_setup` function

``` r
racled_setup(key = "****************1234",
             email = "you@youremail.com")
```

We can now query and read conflict data from Mali, it returns a
`data.frame`

``` r
library(racled)
mali_conflicts <- read_acled("mali")
str(mali_conflicts)
## 'data.frame':    5177 obs. of  31 variables:
##  $ data_id         : int  8366858 8366853 8366854 8366855 8366859 8366452 8366849 8366885 8366889 8366447 ...
##  $ iso             : int  466 466 466 466 466 466 466 466 466 466 ...
##  $ event_id_cnty   : chr  "MLI5307" "MLI5305" "MLI5302" "MLI5303" ...
##  $ event_id_no_cnty: int  5307 5305 5302 5303 5306 5304 5296 5294 5299 5295 ...
##  $ event_date      : chr  "2021-07-01" "2021-06-30" "2021-06-30" "2021-06-30" ...
##  $ year            : int  2021 2021 2021 2021 2021 2021 2021 2021 2021 2021 ...
##  $ time_precision  : int  1 1 1 1 1 1 1 1 1 1 ...
##  $ event_type      : chr  "Battles" "Strategic developments" "Violence against civilians" "Strategic developments" ...
##  $ sub_event_type  : chr  "Armed clash" "Looting/property destruction" "Attack" "Looting/property destruction" ...
##  $ actor1          : chr  "Katiba Macina" "Islamic State (West Africa) - Greater Sahara Faction" "Al Mourabitoune Battalion" "Unidentified Armed Group (Mali)" ...
##  $ assoc_actor_1   : chr  "JNIM: Group for Support of Islam and Muslims" "" "JNIM: Group for Support of Islam and Muslims" "" ...
##  $ inter1          : int  2 2 2 3 2 2 3 6 2 2 ...
##  $ actor2          : chr  "Dozo Communal Militia (Mali)" "Civilians (Mali)" "Civilians (Mali)" "Civilians (Mali)" ...
##  $ assoc_actor_2   : chr  "" "" "MINUSMA: United Nations Multidimensional Integrated Stabilization Mission in Mali (2020-); Labour Group (Mali)" "MINUSMA: United Nations Multidimensional Integrated Stabilization Mission in Mali (2020-); Labour Group (Mali)" ...
##  $ inter2          : int  4 7 7 7 7 1 2 0 7 7 ...
##  $ interaction     : int  24 27 27 37 27 12 23 60 27 27 ...
##  $ region          : chr  "Western Africa" "Western Africa" "Western Africa" "Western Africa" ...
##  $ country         : chr  "Mali" "Mali" "Mali" "Mali" ...
##  $ admin1          : chr  "Mopti" "Gao" "Gao" "Gao" ...
##  $ admin2          : chr  "Djenne" "Gao" "Bourem" "Ansongo" ...
##  $ admin3          : chr  "Femaye" "Anchawadi" "Tarkint" "Bara" ...
##  $ location        : chr  "Koumaga" "In Aoutel" "Almoustarat" "Bara" ...
##  $ latitude        : num  14 16.1 17.4 15.8 13.6 ...
##  $ longitude       : num  -4.399 0.753 0.09 0.325 -3.709 ...
##  $ geo_precision   : int  1 1 2 2 1 1 1 1 2 1 ...
##  $ source          : chr  "Signal" "Signal" "Signal" "Signal" ...
##  $ source_scale    : chr  "New media" "New media" "New media" "New media" ...
##  $ notes           : chr  "On 1 July 2021, Katiba Macina (JNIM) militants attacked the village of Koumaga-Marka [Koumaga] (Djenne, Mopti)."| __truncated__ "Looting: On 30 June 2021, ISWAP (Greater Sahara) militants seized ruminants in Atolate [In Aoutel] (Gao, Gao)." "On 30 June 2021, suspected Al-Mourabitoune (JNIM) militants opened fire against a MINUSMA logistics convoy near"| __truncated__ "Looting: On 30 June 2021, unknown gunmen pillaged and damaged a truck transporting food supplies for MINUSMA ne"| __truncated__ ...
##  $ fatalities      : int  0 0 1 0 0 2 2 0 0 0 ...
##  $ timestamp       : int  1625510721 1625510721 1625510721 1625510721 1625510721 1625510720 1625510721 1625510721 1625510721 1625510720 ...
##  $ iso3            : chr  "MLI" "MLI" "MLI" "MLI" ...
```

## Acknowledgements

This project makes use of data from the [Armed Conflict Location and
Event Data Project (ACLED)](https://www.acleddata.com/). The data is
invaluable for providing real-time information on political violence and
protests, which enhances the functionality and reliability of the
`racled` package. We would like to extend our gratitude to the ACLED
team for their efforts in collecting and maintaining this data. Please
note that the ACLED data is used in accordance with their [terms of
service](https://acleddata.com/download/35181/). [ACLED API User
Guide](https://acleddata.com/acleddatanew/wp-content/uploads/2020/10/ACLED_API-User-Guide_2020.pdf)
for more details on how to interact with their API.
